## Organization chart  application 

### User Story:
The company you work for, A.inc, wants to keep their Organization Chart up to date, so they can clearly determine each worker's role and responsibilities, including management chain up to the CEO. They have assigned to you the awesome task of writing a program to help them achieve this. 

###Requirements:

  * For every employee, store (at least) the following information:
   - First name(s), last name(s), role, start date and employee number.
   - The available roles are:
      - CEO
      - Vice President
      - Director
      - Manager
      - Employee
   - Every active employee within the organization has a manager, except the CEO. 
  * There is only a single CEO.
  * Every active employee has a numeric value that determines their level in the company hierarchy in relation to the CEO (who is at level 1)
   - For instance, the CEO is at level 1, one of his direct reports is at level 2, the direct report of the latter is at level 3 and so on. 
  * Adding an employee to the Organization
  *  Changing teams
     - An employee can move to another team  within the organization. When moving to a different team, an employee starts reporting to a new manager, without transferring his past subordinates to the new team. Instead, the most senior (based on start date) of his past subordinates should be promoted to manage the employee’s former team.
     - Exmaple 
      - Louis, who used to report to Sally, decides to move teams, and now he reports to Brandon. 
      - Mark, who was Louis’ most senior direct report, is promoted to manage all former subordinates of Louis, and he also now reports to Sally. 
  *  	Employee goes on holidays
     - When an employee goes on holidays, all his subordinates start reporting to the employee's manager temporarily. 
   - Exmaple 
    - Pete, who reports to Laura, goes on holidays. Until he comes back, all his direct reports now report to Laura

### Endpoints 
This code has jhisper generated codes. 

* **GET /api/employees:** Endpoint serving all active employees 
* **GET /api/employees/{id}:** Endpoint serving details of one employee 
* **PUT /api/employee/{id}:** Endpoint to update employee details including manager ID(changing teams)
* **POST /api/employee/:** Endpoint to create new employee 
* **DELETE  /api/employee/{id} :** Endpoint to create one employee 

### Setting up project 
#### Prerequisites : Mysql 5.6+ , Java 1.7+ ,Docker

  * Create DB by running ./src/db/create_user.sql as mysql 'root' user
  * Login into 'orgchart' DB and run ./src/db/schema.sql to create seed data
  * Start jhipster config by running command
     - docker-compose -f src/main/docker/jhipster-registry.yml up
  * Start app by running command 
     - java -jar  ./lib/orgchart-0.0.1-SNAPSHOT.jar
  * jhipster config server will be served at  http://localhost:8761/
  * Org chart  will be served at  http://localhost:8081
  
### To use app 
   - Get authentication token by logging into config server using 'admin/admin' as login credentials .Use 'api​/authenticate' to get the JWT token 
   -  Add token on http header as a 'Bearer Token' 
