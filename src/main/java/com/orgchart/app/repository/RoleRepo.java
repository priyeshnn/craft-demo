package com.orgchart.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orgchart.app.domain.Role;


@Repository
public interface RoleRepo extends JpaRepository<Role,Long>{
	
	Role findByName(String name);
}
