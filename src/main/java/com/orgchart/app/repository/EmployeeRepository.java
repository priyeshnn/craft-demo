package com.orgchart.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
  

import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;

 
@Repository
public interface EmployeeRepository  extends JpaRepository<Employee,Long> {
	public List<Employee> findByMgrId(Long mgrId);
	public List<Employee> findByRole(Role role);
}
