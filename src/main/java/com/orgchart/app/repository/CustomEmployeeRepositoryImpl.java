package com.orgchart.app.repository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.orgchart.app.domain.Employee;

@Component
public class CustomEmployeeRepositoryImpl {
//implements CustomEmployeeRepository {

//	@Autowired
//    EmployeeRepository empRepo;
//	public short findLevel(Long mgrId) {
//		
//		if (mgrId ==null) {
//    		return 1;
//    	}
//		
//    	Optional<Employee> mgr=empRepo.findById(mgrId);
//    	if (!mgr.isPresent()) {
//    		return -1;
//    	}
//    	return (short) (mgr.get().getLevel()+1);
//	}
//	 
//	public List<Employee> findReportees(Long mgrId){
//		 
//		   List<Employee> reportees =empRepo.findByMgrId(mgrId);
//		   Comparator <Employee> cmp = new Comparator<Employee>() {
//			   public int compare(Employee e1, Employee e2 ) {
//				     return e1.getsDate().compareTo(e2.getsDate());
//			   };
//		   };
//		   Collections.sort(reportees,cmp);
//		   return reportees;
//	 }
//	
//	void updateReportees( List<Employee> reportees,Employee mgr ) {
//		if (reportees.size()<1) {
//			return;
//		}
//		for(Employee rptee:reportees) {
//			rptee.setLevel( (short)(mgr.getLevel()+1));
//			rptee.setMgrId(mgr.getId());
//			rptee=empRepo.save(rptee);
//		}
//	}
//	@Transactional
//	public Employee moveTeam(Employee currEmp , Employee empUpdtd){
//		
//		   System.out.println("Current emp id" + currEmp.getId()  + " new mgr id" + empUpdtd.getMgrId());
//		   List<Employee> reportees =findReportees(currEmp.getId());
//		   
//		   short level=findLevel(empUpdtd.getMgrId());
//		   System.out.println("new level of emp beeing updated" + level);
//		   
//		   empUpdtd.setLevel(level);
//		   empUpdtd = empRepo.save(empUpdtd);
//		   if (reportees.size()<1) {
//			   return empUpdtd;
//		   }
//		   Employee  seniorMostReportee =reportees.get(0);
//		   reportees.remove(0);
//		   level=findLevel(currEmp.getMgrId());
//		   seniorMostReportee.setLevel(level);
//		   seniorMostReportee.setMgrId(currEmp.getMgrId());
//		   seniorMostReportee=empRepo.save(seniorMostReportee);
//		   
//		   updateReportees(reportees,seniorMostReportee);
//		   return empUpdtd;
//	 }
}
