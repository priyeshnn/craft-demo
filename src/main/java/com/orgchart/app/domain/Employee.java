package com.orgchart.app.domain;

 
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    public Long id;
	 	
	 
	 	private Long  mgrId;
	 	
		@Transient
	 	public transient Long tmpMgrId; 
		
		public Long getTmpMgrId() {
			return tmpMgrId;
		}

		public void setTmpMgrId(Long tmpMgrId) {
			this.tmpMgrId = tmpMgrId;
		}


		@Transient
	 	public transient short level; 
	 	
	 	public Long getMgrId() {
			return mgrId;
		}

		public void setMgrId(Long mgrId) {
			this.mgrId = mgrId;
		}

		public short getLevel() {
			return level;
		}

		public void setLevel(short level) {
			this.level = level;
		}

 
	 	@NotNull
	    @Size(max = 80)
	    @Column(name = "first_name", length = 80, nullable = false)
	    private String firstname;
	 	
	 	@NotNull
	    @Size(max = 80)
	    @Column(name = "last_name", length = 80, nullable = false)
	      private String lastname;
	 	
	 	  public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getFirstname() {
			return firstname;
		}

		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}

		public String getLastname() {
			return lastname;
		}

		public void setLastname(String last_name) {
			this.lastname = last_name;
		}

		public Role getRole() {
			return role;
		}

		public void setRole(Role role) {
			this.role = role;
		}

	 
		 

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		  @NotNull
		  @JoinColumn(name = "role_id", unique = true)
	      @OneToOne()
	      private Role role;
		  
		  
	 	
	 	  
	      @Column(name = "start_date", nullable = false)
	 	  @JsonFormat
	      (shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy")
	      private Date sDate ;
	 	 
 
	      public Date getsDate() {
			return sDate;
		  }

		   public void setsDate(Date sDate) {
				this.sDate = sDate;
		   }


		@Column(name = "is_on_pto", nullable = false)
		   boolean on_pto=false;
		public boolean isOn_pto() {
			return on_pto;
		}

		public void setOn_pto(boolean on_pto) {
			this.on_pto = on_pto;
		}

}
