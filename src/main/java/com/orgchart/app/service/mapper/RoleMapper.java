package com.orgchart.app.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;
import com.orgchart.app.service.dto.EmployeeDTO;
import com.orgchart.app.service.dto.RoleDTO;

@Mapper(componentModel = "spring", uses = {})
public interface RoleMapper {
	 Role 	 roleDTOtoRoleDTO(RoleDTO dto);
	 default Role toRole(Long id) {
	        if (id == null) {
	            return null;
	        }
	        Role role = new Role();
	        role.setId(id);
	        return role;
	    }
}
