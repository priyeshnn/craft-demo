package com.orgchart.app.service.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
 
import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;
import com.orgchart.app.service.dto.EmployeeDTO;
import com.orgchart.app.service.dto.RoleDTO;

@Mapper(componentModel = "spring", uses = {RoleMapper.class })
public interface EmployeeMapper {
	
		@Mappings({ 
					@Mapping (source="role.id",target="role_id"  ),
					@Mapping (source="sDate",target="startdate"  )
		})
	    EmployeeDTO employeeToEmployeeDTO(Employee entity);
		
	    @Mappings({ 
				  @Mapping(source = "role_id", target = "role"),
				  @Mapping (source="startdate",target="sDate"  )
	    })
	    Employee 	 employeeDTOtoEmployee(EmployeeDTO dto)  ;
	    
		public List <EmployeeDTO> employeeToEmployeeDTO(List<Employee> entityList);

		default Date toDate(String startdate) throws ParseException {
			SimpleDateFormat smp  = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss") ;
			TimeZone tz = Calendar.getInstance().getTimeZone();
			smp.setTimeZone(tz);
	        Date dt= smp.parse(startdate);
	        return dt;
	    }
		
		default String dateToString(Date dt) throws ParseException {
	        SimpleDateFormat smf  = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss") ;
	        return smf.format(dt);
	    }
		
		default Employee getEmp(Long id) {
	        if (id == null) {
	            return null;
	        }
	        Employee emp = new Employee();
	        emp.setId(id);
	        return emp;
	    }
		
	  default void initEmp(Employee empToBeUpdtd , Employee emp) {
					empToBeUpdtd.setFirstname(emp.getFirstname());
					empToBeUpdtd.setLastname(emp.getLastname());
					empToBeUpdtd.setId(emp.getId());
					empToBeUpdtd.setRole(emp.getRole());
					empToBeUpdtd.setsDate(emp.getsDate());
					empToBeUpdtd.setMgrId(emp.getMgrId());
					empToBeUpdtd.setOn_pto(emp.isOn_pto());
					empToBeUpdtd.setTmpMgrId(emp.getTmpMgrId());
		}
		default Employee customEmpDTOtoEmp(Employee emp,EmployeeDTO dto) throws ParseException {
	        
			 
			Employee empToBeUpdtd = new Employee();
			initEmp(empToBeUpdtd,emp);
			
			if (dto.getFirstname() !=null) {
	        	empToBeUpdtd.setFirstname(dto.getFirstname());
	        }
	        if (dto.getLastname() !=null) {
	        	empToBeUpdtd.setLastname(dto.getLastname());
	        }
	        
	        if (dto.getRole_id() !=null) {
	        	empToBeUpdtd.getRole().setId(dto.getRole_id());
	        }
	        
	        if (dto.getStartdate() !=null) {
	        	empToBeUpdtd.setsDate(toDate(dto.getStartdate()));
	        }
	        
	        if (dto.getMgrId() !=null) {
	        	empToBeUpdtd.setMgrId(dto.getMgrId());
	        }
	        
	        if (dto.isOn_pto() !=null) {
	        	empToBeUpdtd.setOn_pto(dto.isOn_pto());
	        }
	        return empToBeUpdtd;
		}

  
}
