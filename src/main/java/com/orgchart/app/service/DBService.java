package com.orgchart.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.orgchart.app.domain.Employee;
import com.orgchart.app.repository.EmployeeRepository;
import com.orgchart.app.repository.RoleRepo;
import com.orgchart.app.service.mapper.EmployeeMapper;

@Service
public class DBService {
	
	@Autowired
	EmployeeRepository    empRepo;
	
	@Autowired
	RoleRepo 		  	  roleRepo;
	
	 
	
	public EmployeeRepository getEmpRepo() {
		return empRepo;
	}

	public void setEmpRepo(EmployeeRepository empRepo) {
		this.empRepo = empRepo;
	}

	public RoleRepo getRoleRepo() {
		return roleRepo;
	}

	public void setRoleRepo(RoleRepo roleRepo) {
		this.roleRepo = roleRepo;
	}
	
	Employee saveEmployee (Employee e){
		return empRepo.save(e);
		 
	}
	
	List<Employee> saveEmployees (List<Employee> emps){
		return empRepo.saveAll(emps);
	}
     
	 void deleteEmpById(Long id) {
		   empRepo.deleteById(id);
	 }
	 

}
