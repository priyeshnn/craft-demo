package com.orgchart.app.service.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

public class EmployeeDTO implements Serializable{
	   	 
			private static final long serialVersionUID = 1L;
			
	        private Long id;
	        
			public Long getId() {
				return id;
			}

			public void setId(Long id) {
				this.id = id;
			}
			private Long  mgrId;
		 	 

			public Long getMgrId() {
				return mgrId;
			}

			public void setMgrId(Long mgrId) {
				this.mgrId = mgrId;
			}

			public short getLevel() {
				return level;
			}

			public void setLevel(short level) {
				this.level = level;
			}

			private short level;
			
		    private String firstname;
		    private String lastname;
		    private Long role_id;
		    private Boolean on_pto;
		  
		    
		 	public Long tmpMgrId; 
			
		  
			 
			public Long getTmpMgrId() {
				return tmpMgrId;
			}

			public void setTmpMgrId(Long tmpMgrId) {
				this.tmpMgrId = tmpMgrId;
			}

			public Boolean isOn_pto() {
				return on_pto;
			}

			public void setOn_pto(Boolean on_pto) {
				this.on_pto = on_pto;
			}
			private String startdate;
			public void setStartdate(String startdate) {
				this.startdate = startdate;
			}

			public String getStartdate() {
				return startdate;
			}
			public Long getRole_id() {
				return role_id;
			}

			public void setRole_id(Long role_id) {
				this.role_id = role_id;
			}

			public String getLastname() {
				return lastname;
			}

			public void setLastname(String lastname) {
				this.lastname = lastname;
			}

			@Override
		    public String toString() {
		        return "Employees{" +
		            "name=" + getFirstname() +
		           
		            "}";
		    }

			public String getFirstname() {
				return firstname;
			}

			public void setFirstname(String firstname) {
				this.firstname = firstname;
			}	
}
