package com.orgchart.app.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.cache.Cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.stereotype.Service;

import com.orgchart.app.domain.Employee;
 

 
@Service
public class CacheService {


		@Autowired
		JCacheCacheManager empCmgr;
		
		@Autowired
		DBService    dbService;

		Cache<Object,Object>  empCache;
		
		CacheService(JCacheCacheManager empCmgr , DBService dbService){
			this.empCmgr=empCmgr;
			this.dbService=dbService;
			this.empCache=empCmgr.getCacheManager().getCache("employeeCache");
		}
	 
		@Cacheable(value="employeeCache")
		List<Employee> getAll(){
			Iterator<Cache.Entry<Object,Object>> it=this.empCache.iterator();
			List<Employee> employees = new ArrayList<>() ;
			while(it.hasNext()) {
				  Node node=(Node) it.next().getValue();
				  employees.add(node.getEmp());
			}
			return employees;
		}
		
		@CachePut(value="employeeCache",key = "#node.emp.id")
		public Node set(Node node){
			if (node.getEmp().getMgrId() ==null) {
				node.getEmp().setLevel ((short)1);
			} 
			return node;
		}
	    
		@CacheEvict(value="employeeCache",key = "#node.emp.id")
		public void remove(Node node){}
		
	    boolean hasConnected(Node start, Node end,Set<Node> visited) {
	    	 if (start ==end) return true;
	    	 
	    	 visited.add(start);
			 for (Node child:start.getReportees()) {
				  if (child ==end) {
					  return true;
				  }
				  if (!visited.contains(child)) {
					   visited.add(child);
					   if( hasConnected(child,end,visited)) {
						   return true;
					   }
				  }
			 }
			 return false;
	    }
		@Cacheable(value="employeeCache",key = "#id",unless = "#result == null")
		public Node  get(Long id){
			Optional<Employee> e=dbService.getEmpRepo().findById(id);
			return (e.isPresent()?new Node(e.get()):null);
		}	
 
		@Cacheable(value="employeeCache",key = "#emp.id")
		public Node put(Employee emp ){
			return new Node(emp);
		}
		
		@CachePut	(value="employeeCache",key = "#node.emp.id")
		public Node put(Node node,Node mgr ){
			if (mgr ==null) {
			   node.getEmp().setLevel ((short)1);
			}
			if (mgr !=null ) {
				mgr.getReportees().add(node);
			}
			return node;
		}
		
		public void setLevel(Node node) {
			 for(Node n: node.getReportees()) {
				 n.emp.setLevel((short)(node.emp.getLevel()+1));
				 setLevel(n);
				  
			 }
		}
		void setTempMgr(List<Node> nodes) {
			for (Node node:nodes) {
				 setTempMgr(node);
			}
		}
		void updateAllRportees(Node node, Long id,List<Employee> updtdEmps){
			
			if ( ((!node.getEmp().equals(updtdEmps.get(0))) &&
				  (!node.getEmp().isOn_pto()) ) || 
				    node.getEmp().getMgrId() ==null) {
			 	    return;
			}
			//If managerID of the current node if same as current node Id then return-no more updates required
			if (node.getEmp().getMgrId().equals(node.getEmp().getId())) {
				return;
			}
			for (Node n:node.getReportees()) {
			      n.getEmp().setTmpMgrId(id);
			      updtdEmps.add(n.getEmp());
			      updateAllRportees(n,id,updtdEmps);//update all indirect reportees
		    }
		}

		void setTempMgr(Node node) {
			Node mgr=getTempMgr(node);
			for (Node n:node.getReportees()) {
				      n.getEmp().setTmpMgrId(mgr.getEmp().getId());
			}
		}
		void setTempMgr(Node node,List<Employee> updtdEmps) {
			Node mgr=getTempMgr(node);
			updateAllRportees(node,mgr.getEmp().getId(),updtdEmps); 
		}
		
		Node getTempMgr(Node node) {
			if  (!node.getEmp().isOn_pto() || node.getEmp().getMgrId() ==null) {
				return node;
			}
			return getTempMgr((Node)this.empCache.get(node.getEmp().getMgrId()));
		}
		void updateLevel(List<Node> ceos){
			 for(Node ceo:ceos) {
				 setLevel(ceo);
			 }
		}
		
		public static class Node {
		
			public Employee getEmp() {
				return emp;
			}
			public void setEmp(Employee emp) {
				this.emp = emp;
			}
			public ArrayList<Node> getReportees() {
				return reportees;
			}
			public void setReportees(ArrayList<Node> reportees) {
				this.reportees = reportees;
			}
			Employee emp;
			short level ;
			public short getLevel() {
				return level;
			}
			public void setLevel(short level) {
				this.level = level;
			}
			ArrayList<Node> reportees  = new ArrayList<>() ;
			Node(Employee emp){
				 this.emp=emp;
			}
	    }
	
}
