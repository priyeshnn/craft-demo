package com.orgchart.app.service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
 

import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;
import com.orgchart.app.service.CacheService.Node;

@Component
public class ServiceMgr implements ApplicationListener<ApplicationReadyEvent>{


	public CacheService getCacheService() {
		return cacheService;
	}

	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}

	public DBService getDbService() {
		return dbService;
	}

	public void setDbService(DBService dbService) {
		this.dbService = dbService;
	}

	@Autowired
	CacheService cacheService;
	
	@Autowired
	DBService dbService;
	
	@Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
		initCache();
	}
	void initCache(){
		
		List<Employee> employees=dbService.getEmpRepo().findAll();
		List<Node> ceos= new ArrayList<Node>();
		List<Node> employeesOnPTO=new ArrayList<Node>();
    	for (Employee e:employees) {
    		 cacheService.put(e) ;
    	}
    	for (Employee e :employees) {
   		 	Node mgr=e.getMgrId() !=null?cacheService.get(e.getMgrId()):null;
   		 	Node empNode=cacheService.get(e.getId());
	   		if (empNode.getEmp().isOn_pto()) {
				 employeesOnPTO.add(empNode);
			}
   		 	if (mgr ==null) {
   		 		ceos.add(empNode);
   		 	}
   		 	cacheService.put(empNode,mgr);
   	    }
    	cacheService.updateLevel(ceos);
    	cacheService.setTempMgr(employeesOnPTO);
    }
	
	public Employee save (Employee e){
		if ( (e=dbService.saveEmployee(e)) !=null ) {
			  Node mgr=e.getMgrId() !=null?cacheService.get(e.getMgrId()):null;
			  Node newNode=new Node(e);
			  cacheService.put(newNode,mgr); 
			  if (mgr !=null) {
				  newNode.getEmp().setLevel((short)(mgr.getEmp().getLevel()+1));
				  if (mgr.getEmp().isOn_pto()) {
					  cacheService.setTempMgr(mgr);
				  }
			  }
		}
		return e;
	}
	
	public Employee get (Long id){
		Node node= cacheService.get(id);
		return node != null?node.getEmp():null;
	}
	
	Integer getSeniorRptee(List<Node> 	   reportees) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Integer indx=-1;
		Long seniorEmpEpoc= Long.MAX_VALUE;
		for (int i=0;i<reportees.size(); i++) {
			String s=sdf.format(reportees.get(i).getEmp().getsDate());
			Date dt = sdf.parse(s);
		    long epoch = dt.getTime();
		    if(epoch<seniorEmpEpoc) {
		    	seniorEmpEpoc=epoch;
		    	indx=i;
		    }
		}
		return indx;
	}
	
	List<Employee> updateReportees(List<Node> 	   reportees ) throws ParseException{
		
		List<Employee> toBeUpdtd= new ArrayList<>();
		int indx= getSeniorRptee(reportees);
		Employee seniorEmp=reportees.get(indx).getEmp();
		for (int i=0;i<reportees.size();i++) {	
			 if (i ==indx) {
				 continue;
			 }
			
			 Employee e=reportees.get(i).getEmp();
			 reportees.get(indx).getReportees().add( reportees.get(i));
			 e.setMgrId(seniorEmp.getId());
			 toBeUpdtd.add(e);
		}
		toBeUpdtd.add(seniorEmp);
		return toBeUpdtd;
	}
	public List<Employee> update (Employee updEmp) throws ParseException{
		
		List<Node> 	   reportees = new ArrayList<>();
		List<Employee> toBeUpdtd = new ArrayList<>();
		
		toBeUpdtd.add(updEmp);
		Node currNode=cacheService.get(updEmp.getId());
		boolean  oldPTOStatus=currNode.getEmp().isOn_pto();
		
		if (currNode.getEmp().getMgrId() !=null && 
		   !currNode.getEmp().getMgrId().equals(updEmp.getMgrId())) {
			
			reportees=cacheService.get(updEmp.getId()).getReportees();
			currNode.getEmp().setMgrId(updEmp.getMgrId());
			
			if (reportees.size()>0) {
				toBeUpdtd.addAll(updateReportees(reportees));
			}
		}
		
		List<Employee> empUpdtd=dbService.saveEmployees(toBeUpdtd);
		if (  empUpdtd.size()== toBeUpdtd.size() ) {
			
			  currNode.setEmp(updEmp);
			  cacheService.set(currNode);
			  
			  if (toBeUpdtd.size()>1) {
				  Employee senorEmp=toBeUpdtd.get(toBeUpdtd.size()-1);
				  currNode.getReportees().clear();
				  currNode.getReportees().add( cacheService.get(senorEmp.getId()) );
			  }
			  if (updEmp.getMgrId() !=null) {
				  Node mgrNode=cacheService.get(updEmp.getMgrId());
				  currNode.getEmp().setLevel((short)(mgrNode.getEmp().getLevel()+1));
			  }
		      cacheService.setLevel(currNode);
		      
			  for (Node node:reportees) {
				   cacheService.set(node);
			  }
			  if ((oldPTOStatus != updEmp.isOn_pto() ) && 
				   currNode.getReportees().size()>0) {
				   cacheService.setTempMgr(currNode,toBeUpdtd);
			  }
		
		}
		return toBeUpdtd;
	}
	
	 public boolean getRole(Long id) {
		     return dbService.getRoleRepo().findById(id).isPresent();
	 }
	
	 public boolean hasConnected(Long start, Long end) {
		     Node s=cacheService.get(start);
		     Node e=cacheService.get(end);
		     return cacheService.hasConnected(s,e, new HashSet<Node>());
	 }
	 
	 public List<Employee> findAll() {
	     return cacheService.getAll();
	 }
	 
	 void rmFrmMgrRpteesList(Long id, Node       mgr) {
		 
		 
		 for (Node n:mgr.getReportees()) {
			 if (id.equals(n.getEmp().getId())) {
				 mgr.getReportees().remove(id);
				 return;
			 }
		 }
	 }
	 List<Employee> update(Long id) throws ParseException{
		 
		 List<Node> reportees=cacheService.get(id).getReportees();
		 Node       node=cacheService.get(id);
		 Node       mgr=cacheService.get(node.getEmp().getMgrId());
		 List<Employee> updtd=new ArrayList<>();
		 for (Node n:reportees) {
			 Employee e=n.getEmp();
			 e.setMgrId(mgr.getEmp().getId());
			 mgr.getReportees().add(n);
			 updtd.add(e);
			 dbService.getEmpRepo().save(e);
		 }
		 cacheService.setLevel(mgr);
		 cacheService.remove(node);
		 rmFrmMgrRpteesList(id,mgr);
		 if (node.getReportees().size()>0 && mgr.getEmp().isOn_pto()) {
			 updtd.clear();
			 updtd.add(mgr.getEmp());
			 cacheService.setTempMgr(mgr,updtd);
			 updtd.remove(0);
		 }
		 return updtd;
	 }
	 
	 public List<Employee> deleteById(Long id) throws ParseException {
		 List<Employee> updtd=update(id); 
		 
		 update(id);
		 dbService.deleteEmpById(id);
		 return updtd;
	 }
	
 }
	 

