package com.orgchart.app.cache;

 

import java.util.ArrayList;

import javax.cache.CacheManager;

import org.ehcache.Cache;
//import org.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
 
//import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

import com.orgchart.app.config.CacheConfiguration;



@Component
public class OrgGraph   {
	

	@Autowired
	CacheManager cmgr;
	
//	@Cacheable(value="employeeCache",key = "#id")
//	public People  findEmployee(Long id){
//		if (id ==null) return null;
//		//cache.cacheManagerCustomizer();
//		 javax.cache.Cache<Long, People> ch=cmgr.getCache("employeeCache",Long.class,People.class);
//	    System.out.println("called findNodeByMgr" +id + "ch size-" +ch.containsKey(1l) );
//		return ch.get(id);
//	}	
//	
//	@Cacheable(value="employeeCache",key = "#id")
//	public People  createNode(Long id,Long mgrId,boolean isOnPTO){
//		
//		People mgr = findEmployee(mgrId);
//		People people=new People(id,mgrId,isOnPTO);
//		if (mgr !=null ) {
//			System.out.println("added to childrens" );
//			mgr.getReportees().add(people);
//		}
//		javax.cache.Cache<Long, People> ch=cmgr.getCache("employeeCache",Long.class,People.class);
//		ch.put(id, people);
//		System.out.println("created emploee in cache" + id );
//		return people;
//	}
////	
//	public short findLevel(Long id) {
//		 People mgr=findEmployee(id);
//		 System.out.println("findLevel->" +id  + "mgr=" +mgr);
//		 if (mgr ==null) {
//			 return 1;
//		 }
//		 return (short) (1+ findLevel(findEmployee(id).mgrId));
//	}
	
	public static class People {
		
		Long id;
		Long mgrId;
		Boolean isOnPTO;
		ArrayList<People> reportees  = new ArrayList<>() ;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getMgrId() {
			return mgrId;
		}
		public void setMgrId(Long mgrId) {
			this.mgrId = mgrId;
		}
		public Boolean getIsOnPTO() {
			return isOnPTO;
		}
		public void setIsOnPTO(Boolean isOnPTO) {
			this.isOnPTO = isOnPTO;
		}
		public ArrayList<People> getReportees() {
			return reportees;
		}
		public void setReportees(ArrayList<People> reportees) {
			this.reportees = reportees;
		}
		People(Long id, Long mgrId, Boolean isOnPTO){
			 this.id=id;
			 this.mgrId=mgrId;
			 this.isOnPTO=isOnPTO;
		}
		
	}
	
	
	OrgGraph(){
		
	}
 }
