package com.orgchart.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;
import com.orgchart.app.repository.*;
import com.orgchart.app.service.ServiceMgr;
import com.orgchart.app.service.dto.EmployeeDTO;
import com.orgchart.app.service.mapper.EmployeeMapper;
import com.orgchart.app.utils.HttpUtil;

/**
 * Employee controller for managing employees.
 */
@RestController
@RequestMapping("/api")
public class EmployeeResource {
	 
		private final EmployeeRepository    empRepo;
		private final EmployeeMapper  empMapper;
		private final RoleRepo 		  roleRepo;

     	private final ServiceMgr serviceMgr;
		public EmployeeResource(ServiceMgr serviceMgr,EmployeeRepository empRepo, EmployeeMapper empMapper ,RoleRepo roleRepo) {
			this.empRepo=empRepo;
			this.empMapper=empMapper;
			this.roleRepo=roleRepo;
			this.serviceMgr=serviceMgr;
		}
		
	   @GetMapping("/employees")
	   public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
	 		HttpHeaders responseHeaders = new HttpHeaders();
 	        List<Employee>  employees= serviceMgr.findAll();
	        return new ResponseEntity<>(empMapper.employeeToEmployeeDTO(employees), responseHeaders, HttpStatus.OK);
	    }

	    /**
	     * POST  / Create a new employee.
	     * @param  EmployeeDTO 
	     * @return Response with status 201 (Created)
	     * @throws URISyntaxException  
	     */
	    @PostMapping("/employee")
	    public ResponseEntity<EmployeeDTO> createEmployee(@Valid @RequestBody EmployeeDTO empDTO) throws URISyntaxException  {
	         
	    	Optional<Role> role=roleRepo.findById(empDTO.getRole_id());
		    if ( empDTO.getMgrId() == null && role.isPresent() && 
		        	 !role.get().getName().equals("CEO") ) {
		        	 return ResponseEntity.badRequest().body(null);
		    }
		    
		    
		    if (empDTO.getId() != null  ) {
		        return ResponseEntity.badRequest().body(null);
		    }
		    
		    if (  empDTO.getMgrId() != null && 
		    	 !empRepo.findById(empDTO.getMgrId()).isPresent()) {
		          return ResponseEntity.badRequest().body(null);
		    }
		    Employee emp;
	        emp= empMapper.employeeDTOtoEmployee(empDTO);
	        emp = serviceMgr.save(emp);
	        EmployeeDTO result = empMapper.employeeToEmployeeDTO(emp);
	        return ResponseEntity.created(new URI("/api/employee/" + emp.getId()))
	            .body(result);
	    }
	    
	    /**
	     * PUT / Updates existing employee.Does not create new employees if id does not exist
	     * @param  EmployeeDTO 
	     * @return Array of EmployeeDTO , response with status 201 (Created) .Bad requests 400
	     * @throws ParseException  
	     */
	    @PutMapping("/employee/{id}")
	    public ResponseEntity<List<EmployeeDTO>> updateEmployee(@PathVariable Long id,@Valid @RequestBody EmployeeDTO empDTO) throws ParseException {
	        
	    	  Employee emp = serviceMgr.get(id);
	    	  if (emp ==null) {
	    		  return ResponseEntity.badRequest().body(null);
	    	  }
	    	  
	    	  if (empDTO.getMgrId() !=null && serviceMgr.get(empDTO.getMgrId())  ==null ) {
	    		  return ResponseEntity.badRequest().body(null);
	    	  }
	    	  
	    	  if (empDTO.getRole_id() !=null && !serviceMgr.getRole(empDTO.getRole_id() )) {
	    		  return ResponseEntity.badRequest().body(null);
	    	  }
	    	  
		      if (empDTO.getMgrId() !=null &&
		    	  serviceMgr.hasConnected(id, empDTO.getMgrId())) {
		          return ResponseEntity.badRequest().body(null);
		      }
		      emp=empMapper.customEmpDTOtoEmp(emp,empDTO);
		      List<Employee> empUpdtd = serviceMgr.update(emp);
		      List<EmployeeDTO> result = empMapper.employeeToEmployeeDTO(empUpdtd);
	          return ResponseEntity.ok()
		            .body(result);
	    }
	    
	    /**
	     * DELETE / Delete existing employee. 
	     * @param  Employee Id 
	     * @return Response with status 201 (Created) without any data . 
	     *         Bad requests returned with 400
	     * @throws ParseException  
	     */
	    @DeleteMapping("/employee/{id}")
	    public ResponseEntity<List<EmployeeDTO>> deleteEmployee(@PathVariable Long id) throws ParseException {
	    	
	    	if ( serviceMgr.get(id) ==null || serviceMgr.get(id).getMgrId() ==null) {
	    		 return ResponseEntity.badRequest()
	    		            .body(null);
	    	}
	    	List<EmployeeDTO> result = empMapper.employeeToEmployeeDTO(serviceMgr.deleteById(id));
	        return ResponseEntity.ok().body(result);
	    }

}
