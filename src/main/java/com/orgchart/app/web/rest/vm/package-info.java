/**
 * View Models used by Spring MVC REST controllers.
 */
package com.orgchart.app.web.rest.vm;
