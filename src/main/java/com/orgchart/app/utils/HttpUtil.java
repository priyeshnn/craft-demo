package com.orgchart.app.utils;

import org.springframework.http.HttpHeaders;

public class HttpUtil {

	 public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
	       
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("X-orgchart-error", defaultMessage);
	        headers.add("X-orgchart-params", entityName);
	        return headers;
	    }
}
