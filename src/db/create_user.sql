create database if not exists orgchart;
CREATE USER 'orgmgr'@'%' IDENTIFIED BY 'orgmgr';
grant all privileges on *.* to 'orgmgr'@'%'  with grant option;
