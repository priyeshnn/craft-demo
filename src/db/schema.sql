DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS role;
CREATE TABLE employee
(
    id              INTEGER NOT NULL AUTO_INCREMENT,
    role_id         INTEGER NOT NULL,
    first_name      CHAR VARYING(80)  NOT NULL,
    last_name       CHAR VARYING(80)  NOT NULL,
    start_date      DATETIME NOT NULL,
    mgr_id          INTEGER ,
    is_on_pto       BOOLEAN,
    FOREIGN KEY (role_id)
        REFERENCES role(id)
    ON DELETE CASCADE,
    CONSTRAINT id_pk PRIMARY KEY (
            id
    )
)ENGINE=MyISAM;

CREATE INDEX mgr_id_idx ON employee (
       mgr_id
);
CREATE TABLE role
(
    id                  INTEGER NOT NULL AUTO_INCREMENT,
    name                CHAR VARYING(50)  NOT NULL,
    CONSTRAINT id_pk PRIMARY KEY (
            id
    )
)ENGINE=MyISAM;

insert into role (name) values ('CEO');
insert into role (name) values ('Manager');
insert into role (name) values ('Vice President');
insert into role (name) values ('Employee');
insert into role (name) values ('Manager');    
