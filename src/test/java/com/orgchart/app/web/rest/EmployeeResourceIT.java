package com.orgchart.app.web.rest;
 
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orgchart.app.OrgchartApp;
import com.orgchart.app.domain.Employee;
import com.orgchart.app.domain.Role;
import com.orgchart.app.repository.EmployeeRepository;
import com.orgchart.app.repository.RoleRepo;
import com.orgchart.app.service.ServiceMgr;
import com.orgchart.app.service.dto.EmployeeDTO;
import com.orgchart.app.service.mapper.EmployeeMapper;
import com.orgchart.app.web.rest.errors.ExceptionTranslator;
import com.orgchart.app.web.rest.EmployeeResource;

 

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrgchartApp.class)
@Rollback(false)
public class EmployeeResourceIT {

		@Autowired
		EmployeeRepository   empRepo;
		@Autowired
		RoleRepo     		 roleRepo;
		@Autowired
		ServiceMgr 			 serviceMgr;
		@Autowired
		EmployeeMapper 		 empMapper;
		@Autowired
		private ExceptionTranslator 				  exceptionTranslator;
		@Autowired
		private MappingJackson2HttpMessageConverter   jacksonMessageConverter;
		public    Map<String,Employee>    testEmployees;
		private MockMvc      empMockMvc;
		@Autowired
		private ObjectMapper objectMapper;
		
        public static int totalRecBefore =0;
	    @Autowired
	    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
		
	     
	    
	    @BeforeEach
	    public   void initTest() {
	    	testEmployees = initEmp();
	    }
	    @BeforeEach
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	        EmployeeResource empResource = new EmployeeResource(serviceMgr,empRepo,empMapper, roleRepo);
	        this.empMockMvc = MockMvcBuilders
	        				  .standaloneSetup(empResource)
	        				  .setCustomArgumentResolvers(pageableArgumentResolver)
	        				  .setControllerAdvice(exceptionTranslator)
	        		          .setMessageConverters(jacksonMessageConverter).build();
	    }  
	    
 
	    public  Map<String,Employee> initEmp() {
	    	Map<String,Employee>  testEmployees= new HashMap<>();
	        Employee ceo = new Employee();
	        Role roleCEO= new  Role();
	        Role roleEmp= new  Role();
	        Long ceoId=roleRepo.findByName("CEO").getId();
	        Long sweId=roleRepo.findByName("Employee").getId();
	        roleEmp.setId(sweId);
	        roleCEO.setId(ceoId);
	        
	        ceo.setFirstname("CEO");
	        ceo.setLastname("CEO");
	        
	        ceo.setRole(roleCEO);
	        ceo.setMgrId(null);
	        ceo.setOn_pto(false);
	        ceo.setsDate(new Date());
	        testEmployees.put("ceo",ceo);
	       
	        Employee emp1 = new Employee();
	        emp1.setFirstname("emp1");
	        emp1.setLastname("emp1");
	        emp1.setRole(roleEmp);
	        emp1.setOn_pto(false);
	        emp1.setsDate(new Date());
	        testEmployees.put("emp1",emp1);
	        
	        Employee emp2 = new Employee();
	        emp2.setFirstname("emp2");
	        emp2.setLastname("emp2");
	        emp2.setRole(roleEmp);
	        emp2.setOn_pto(false);
	        emp2.setsDate(new Date());
	        testEmployees.put("emp2",emp2);
	        
	        Employee emp3 = new Employee();
	        emp3.setFirstname("emp3");
	        emp3.setLastname("emp3");
	        emp3.setRole(roleEmp);
	        emp3.setOn_pto(false);
	        emp3.setsDate(new Date());
	        testEmployees.put("emp3",emp3);
	        
	        Employee emp4 = new Employee();
	        emp4.setFirstname("emp4");
	        emp4.setLastname("emp4");
	        emp4.setRole(roleEmp);
	        emp4.setOn_pto(false);
	        emp4.setsDate(new Date());
	        testEmployees.put("emp4",emp4);
	        
	        Employee emp5 = new Employee();
	        emp5.setFirstname("emp5");
	        emp5.setLastname("emp5");
	        emp5.setRole(roleEmp);
	        emp5.setOn_pto(false);
	        emp5.setsDate(new Date());
	        testEmployees.put("emp5",emp5);
	        
	        return testEmployees;
	    }
	    

	    ResultActions performEmployeePostAction(String uri,EmployeeDTO empDTO) throws IOException, Exception{
	    	return empMockMvc.perform(post(uri)
	        		  .contentType(TestUtil.APPLICATION_JSON_UTF8)
	                  .content(TestUtil.convertObjectToJsonBytes(empDTO)))
	    			 .andExpect(status().isCreated());
	    }
	    
	    ResultActions performEmployeePutAction(String uri,EmployeeDTO empDTO) throws IOException, Exception{
	    	return empMockMvc.perform(put(uri)
	        		  .contentType(TestUtil.APPLICATION_JSON_UTF8)
	                  .content(TestUtil.convertObjectToJsonBytes(empDTO)))
	    			 .andExpect(status().isOk());
	    }
	    
	    ResultActions performEmployeeDeleteAction(String uri) throws IOException, Exception{
	    	    return empMockMvc.perform(MockMvcRequestBuilders.delete(uri)
		        		  .contentType(TestUtil.APPLICATION_JSON_UTF8))
		    			 .andExpect(status().isOk());
	        		       
	    }
	    
	    void assetEmployeeDtls(EmployeeDTO respDTO,Employee emp) {
	    	 assertThat(respDTO.getFirstname()).isEqualTo(emp.getFirstname());
	         assertThat(respDTO.getLastname()).isEqualTo(emp.getLastname());
	         assertThat(respDTO.getMgrId()).isEqualTo(emp.getMgrId());
	         assertThat(respDTO.getRole_id()).isEqualTo(emp.getRole().getId());
	    }
	    
	    @Test
	    @Transactional
	    @Order(1)
	    public void createAndUpdateCEO() throws Exception {
	    	
	    	 Employee emp=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(emp); 
	         
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 testEmployees.put("ceo", empMapper.employeeDTOtoEmployee(respDTO));
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,emp); 
	    	
	         emp.setId(respDTO.getId());
	         emp.setFirstname("CEO updated");
	         empDTO = empMapper.employeeToEmployeeDTO(emp); 
	         
	         contentAsString = performEmployeePutAction("/api/employee/"+emp.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTOs[] = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	    	 assertThat(respDTOs.length).isEqualTo(1);
	    	 assetEmployeeDtls(respDTOs[0],emp);
	    	 
	     
	    	 
	        
	    }
     
	    @Test
	    @Transactional
	    public void createNONCEOEmployeesTestEmployeeLevels() throws Exception {
	    	
	 
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setLevel(respDTO.getLevel());
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 //Employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp1.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp2); 
	    	 emp2.setLevel(respDTO.getLevel());
	    	 emp2.setId(respDTO.getId());
	    	 
	    	 //Employee  3 reporting to  Employee 1
	    	 Employee emp3=testEmployees.get("emp3");
	    	 emp3.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp3); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp1.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp3);
	    	 emp3.setLevel(respDTO.getLevel());
	    	 emp3.setId(respDTO.getId());
	    	 
	    	 //Employee  4 reporting to  Employee 3
	    	 Employee emp4=testEmployees.get("emp4");
	    	 emp4.setMgrId(emp3.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp4); 
	         contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp3.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp4);
	    	 
	    	 //Employee  5 reporting to  CEO
	    	 Employee emp5=testEmployees.get("emp5");
	    	 emp5.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp5); 
	         contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp5);
	    	 
	    	 
	    	 
	    }
	    void initLevelAndMgrs(EmployeeDTO empDTOs[]) {
	    	Employee emp2=testEmployees.get("emp2");
	    	Employee emp3=testEmployees.get("emp3");
	    	Employee emp4=testEmployees.get("emp4");
	    	Employee emp5=testEmployees.get("emp5");
	    	for (EmployeeDTO dto:empDTOs) {
	    		  if ( dto.getId().equals(emp2.getId()) ) {
	    			   emp2.setLevel(dto.getLevel());
	    			   emp2.setMgrId(dto.getMgrId());
	    		  }else if (dto.getId().equals(emp3.getId())) {
	    			   emp3.setLevel(dto.getLevel());
	    			   emp3.setMgrId(dto.getMgrId());
	    		  }else if (dto.getId().equals(emp4.getId())) {
	    			   emp4.setLevel(dto.getLevel());
	    			   emp4.setMgrId(dto.getMgrId());
	    		  }else {
	    			   emp5.setLevel(dto.getLevel());
	    			   emp5.setMgrId(dto.getMgrId());
	    		  }
	    		  
	    	 }
	    }
	    
	    void initTempMgr(EmployeeDTO empDTOs[]) {
	    	Employee emp2=testEmployees.get("emp2");
	    	Employee emp3=testEmployees.get("emp3");
	    	Employee emp4=testEmployees.get("emp4");
	    	Employee emp5=testEmployees.get("emp5");
	    	for (EmployeeDTO dto:empDTOs) {
	    		  if ( dto.getId().equals(emp2.getId()) ) {
	    			   emp2.setTmpMgrId(dto.getTmpMgrId());
	    		  }else if (dto.getId().equals(emp3.getId())) {
	    			   emp3.setTmpMgrId(dto.getTmpMgrId());
	    		  }else if (dto.getId().equals(emp4.getId())) {
	    			   emp4.setTmpMgrId(dto.getTmpMgrId());
	    		  }else {
	    			   emp5.setTmpMgrId(dto.getTmpMgrId());
	    		  }
	    		  
	    	 }
	    }
	    
	    @Test
	    @Transactional
	    public void moveTeams() throws Exception {
	
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setLevel(respDTO.getLevel());
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 //Employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp1.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp2); 
	    	 emp2.setLevel(respDTO.getLevel());
	    	 emp2.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee  3 reporting to  Employee 2  -- Senior most among Emp2's reportees  
	    	 Employee emp3=testEmployees.get("emp3");
	    	 emp3.setMgrId(emp2.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp3); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp2.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp3);
	    	 emp3.setLevel(respDTO.getLevel());
	    	 emp3.setId(respDTO.getId());
	    	 
	    	 //Employee  4 reporting to  Employee 2
	    	 Employee emp4=testEmployees.get("emp4");
	    	 emp4.setMgrId(emp2.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp4); 
	         contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp2.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp4);
	    	 emp4.setLevel(respDTO.getLevel());
	    	 emp4.setId(respDTO.getId());
  
	    	 //Employee  5 reporting to  Employee 2
	    	 Employee emp5=testEmployees.get("emp5");
	    	 emp5.setMgrId(emp2.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp5); 
	         contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(emp2.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp5);
	    	 emp5.setLevel(respDTO.getLevel());
	    	 emp5.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee 2 moving under CEO 
	    	 emp2.setMgrId(ceo.getId());
	    	 empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePutAction("/api/employee/"+emp2.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTOs[] = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	    	 assertThat(respDTOs.length).isEqualTo(4);
	    	 initLevelAndMgrs(respDTOs);
 
	    	 
	    	 //Assert manger id and levels of all employees affected with the move
	    	 assertThat(emp2.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
			 assertThat(emp2.getMgrId()).isEqualTo(ceo.getId());
			 assertThat(emp3.getLevel()).isEqualByComparingTo((short)(emp2.getLevel()+1));
			 assertThat(emp3.getMgrId()).isEqualTo(emp2.getId());
			 assertThat(emp4.getLevel()).isEqualByComparingTo((short)(emp3.getLevel()+1));
			 assertThat(emp4.getMgrId()).isEqualTo(emp3.getId());
			 assertThat(emp5.getLevel()).isEqualByComparingTo((short)(emp3.getLevel()+1));
			 assertThat(emp5.getMgrId()).isEqualTo(emp3.getId());
	    }
	    
	    
	    @Test
	    @Transactional
	    public void employeesManagerGoingOnAndOffVaccation() throws Exception {
	    	
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 //Employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp2); ;
	    	 emp2.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee  3 reporting to  Employee 1  --   
	    	 Employee emp3=testEmployees.get("emp3");
	    	 emp3.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp3); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp3);
	    	 emp3.setId(respDTO.getId());
	    	 
	    	 //Employee 1 going on PTO
	    	 emp1.setOn_pto(true);
	    	 empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePutAction("/api/employee/"+emp1.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTOs[] = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	    	 
	    	 assertThat(respDTOs.length).isEqualTo(3);
	    	 initTempMgr(respDTOs); 
	    	 //Assert manger id  all employees affected with the move
      		 assertThat(emp1.getMgrId()).isEqualTo(ceo.getId());
      		 assertThat(emp2.getTmpMgrId()).isEqualTo(ceo.getId());
      		 assertThat(emp3.getTmpMgrId()).isEqualTo(ceo.getId());
      		 
      		//Employee 2 going on vacation
	    	 emp2.setOn_pto(true);
	    	 empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePutAction("/api/employee/"+emp2.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTOs  = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	       	 assertThat(respDTOs.length).isEqualTo(1);
	    	 initTempMgr(respDTOs);
      		 assertThat(emp2.getTmpMgrId()).isEqualTo(ceo.getId());
      		 
      		//Employee 2 coming back from vacation
	    	 emp2.setOn_pto(false);
	    	 empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePutAction("/api/employee/"+emp2.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTOs  = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	       	 assertThat(respDTOs.length).isEqualTo(1);
	    	 initTempMgr(respDTOs);
      		 assertThat(emp2.getTmpMgrId()).isEqualTo(ceo.getId());
      		 
      		 
      		//Employee 1 coming back from vacation
	    	 emp1.setOn_pto(false);
	    	 empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePutAction("/api/employee/"+emp1.getId(),empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTOs  = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	       	 assertThat(respDTOs.length).isEqualTo(3);
	    	 initTempMgr(respDTOs);
	    	 
	    	 assertThat(emp1.getMgrId()).isEqualTo(ceo.getId());
      		 assertThat(emp2.getTmpMgrId()).isEqualTo(emp1.getId());
      		 assertThat(emp3.getTmpMgrId()).isEqualTo(emp1.getId());
      	 
	    }
	    
	    
	    
	    @Test
	    @Transactional
	    public void addNewEmployeeWithManagerOnVacation() throws Exception {
	    	 
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 
	      	 //Employee 1 going on from vacation
		      emp1.setOn_pto(true);
		      empDTO = empMapper.employeeToEmployeeDTO(emp1); 
		      contentAsString = performEmployeePutAction("/api/employee/"+emp1.getId(),empDTO).andReturn().getResponse().getContentAsString();
		 
	    	 
	    	 //New employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp2); ;
	    	 emp2.setTmpMgrId(respDTO.getTmpMgrId());
	    	 emp2.setId(respDTO.getId());
      		 assertThat(emp2.getTmpMgrId()).isEqualTo(ceo.getId());
	    	 
	    	 
	    }
	    
	    
	    
	    
	    @Test
	    @Transactional
	    public void deleteEmployee_TestManagerAndLevelsOfAllReportees() throws Exception {
	    	
		     
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp2); ;
	    	 emp2.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee  3 reporting to  Employee 1  --   
	    	 Employee emp3=testEmployees.get("emp3");
	    	 emp3.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp3); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp3);
	    	 emp3.setId(respDTO.getId());
	    	 
	    	 
	    	 //Employee  4 reporting to  Employee 3  --   
	    	 Employee emp4=testEmployees.get("emp4");
	    	 emp4.setMgrId(emp3.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp4); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp4);
	    	 emp4.setId(respDTO.getId());

	    	 
	    	 //Employee 1 leaving the company 
	    	 contentAsString = performEmployeeDeleteAction("/api/employee/"+emp1.getId()).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTOs[] = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
	    	 initLevelAndMgrs(respDTOs);
	    	 
	    	 Optional<Employee> e=empRepo.findById(emp1.getId());
	    	 emp4=empRepo.findById(emp4.getId()).get();
	    	 assertTrue( false == e.isPresent());
      		 assertThat(emp2.getMgrId()).isEqualTo(ceo.getId());
      		 assertThat(emp2.getLevel()).isEqualByComparingTo( (short) (ceo.getLevel() +1));
      		 assertThat(emp3.getMgrId()).isEqualTo(ceo.getId());
      		 assertThat(emp3.getLevel()).isEqualByComparingTo( (short) (ceo.getLevel() +1));
      		 assertThat(emp4.getMgrId()).isEqualTo(emp3.getId());
     		 assertThat(emp4.getLevel()).isEqualByComparingTo( (short) (emp3.getLevel() +1));
      	 
	    }
 
	    
	    @Test
	    @Transactional
	    public void deletEmployeeWhosManagerOnVacation() throws Exception {
 
	    	 //Create CECO
	    	 Employee ceo=testEmployees.get("ceo");
	         EmployeeDTO empDTO = empMapper.employeeToEmployeeDTO(ceo); 
	    	 String contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 EmployeeDTO respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	         assertThat(respDTO.getLevel()).isEqualByComparingTo((short)1);
	    	 assetEmployeeDtls(respDTO,ceo); 
	    	 ceo.setLevel(respDTO.getLevel());
	    	 ceo.setId(respDTO.getId());
	    	 
	    	 //Employee  1 reporting to  CEO
	    	 Employee emp1=testEmployees.get("emp1");
	    	 emp1.setMgrId(ceo.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp1); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assertThat(respDTO.getLevel()).isEqualByComparingTo((short)(ceo.getLevel()+1));
	    	 assetEmployeeDtls(respDTO,emp1); 
	    	 emp1.setLevel(respDTO.getLevel());
	    	 emp1.setId(respDTO.getId());
	    	 
	    	 
	    	 //New employee  2 reporting to  Employee 1
	    	 Employee emp2=testEmployees.get("emp2");
	    	 emp2.setMgrId(emp1.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp2); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp2); ;
	    	 emp2.setId(respDTO.getId());
 
	    	
      		//New employee  3 reporting to  Employee 2
	    	 Employee emp3=testEmployees.get("emp3");
	    	 emp3.setMgrId(emp2.getId());
	         empDTO = empMapper.employeeToEmployeeDTO(emp3); 
	    	 contentAsString = performEmployeePostAction("/api/employee",empDTO).andReturn().getResponse().getContentAsString();
	    	 respDTO = objectMapper.readValue(contentAsString, EmployeeDTO.class);
	    	 assetEmployeeDtls(respDTO,emp3); ;
	    	 emp3.setId(respDTO.getId());
      		 
	      	 //Employee 1 going on  vacation
		      emp1.setOn_pto(true);
		      empDTO = empMapper.employeeToEmployeeDTO(emp1); 
		      contentAsString = performEmployeePutAction("/api/employee/"+emp1.getId(),empDTO).andReturn().getResponse().getContentAsString();

		      //Employee 2 leaving the company 
		      contentAsString = performEmployeeDeleteAction("/api/employee/"+emp2.getId()).andReturn().getResponse().getContentAsString();
		      EmployeeDTO respDTOs[] = objectMapper.readValue(contentAsString, EmployeeDTO[].class);
		      initLevelAndMgrs(respDTOs);
		      initTempMgr(respDTOs);
		    
		      Optional<Employee> e=empRepo.findById(emp2.getId());
		      
              assertTrue( false == e.isPresent());
	      	  assertThat(emp3.getMgrId()).isEqualTo(emp1.getId());
	      	  assertThat(emp3.getLevel()).isEqualByComparingTo( (short) (emp1.getLevel() +1));
	      	  assertThat(emp3.getTmpMgrId()).isEqualTo(ceo.getId());
	    }
	       
	  
}
